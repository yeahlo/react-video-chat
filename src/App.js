import React, {useEffect, useRef, useState} from 'react';
import './App.css';
import Webcam from "./WebCam";
import User from "./User";

const App = () => {

  return (
    <div className="App">
        <User/>
        <Webcam/>
    </div>
  );
};

export default App;
