import React, {useContext} from 'react';
import {Store, updateUser} from "./store";

const User = () => {
    const {state, dispatch} = useContext(Store);
    return (
        <div className="User">
            <h1>{state.formatedUser}</h1>
            <input onChange={e => dispatch(updateUser(e.currentTarget.value))} defaultValue={state.user} />
        </div>
    );
};

export default User;
