import React, {useEffect, useRef, useState} from 'react';

const constraints = window.constraints = {
    audio: false,
    video: true
};

const Webcam = () => {

    const [stream, setStream] = useState(null);
    const videoRef = useRef();

    useEffect(() => {
        navigator.mediaDevices.getUserMedia(constraints).then(stream => setStream(stream));
    },[]);

    useEffect(() => {

        if(stream) {
            videoRef.current.srcObject = stream;
        }

    }, [stream]);

    return (
        <div className="Webcam">
            <video ref={videoRef} autoPlay playsInline/>
        </div>
    );
};

export default Webcam;
