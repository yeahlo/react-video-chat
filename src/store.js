import React, {useReducer} from 'react'

const UPDATE_USER = 'UPDATE_USER';
const UPDATE_FORMATED_USER = 'UPDATE_FORMATED_USER';

const initialState = {
    user: null,
    formatedUser : null
};

export const updateUser = (payload) => ({
    type: UPDATE_USER,
    payload
});

export const updateFormatedUser = (payload) => ({
    type: UPDATE_FORMATED_USER,
    payload
});

const reducer = (state, action) => {
    switch (action.type) {
        case UPDATE_USER :
            return {
                ...state,
                user : action.payload
            };
        case UPDATE_FORMATED_USER :
            return {
                ...state,
                formatedUser : action.payload
            };
        default:
            return state;
    }
};

const Store = React.createContext();

const middleware = store => next => action => {
        // action before state update
        switch (action.type) {
            case UPDATE_USER :
                store.dispatch(updateFormatedUser('[' + action.payload.split('').join('|') + ']'));
                break;
        }
        next(action);
        // action after state update
};

const compose = (...funcs) => x => funcs.reduceRight((composed, f) => f(composed), x);

const StoreProvider = props => {
    const [state, dispatch] = useReducer(reducer, initialState);

    const middlewareAPI = {
        getState: () => state,
        dispatch: action => dispatch(action)
    };

    const middlewares = [middleware];

    const chain = middlewares.map(middleware => middleware(middlewareAPI));

    const enhancedDispatch = compose(...chain)(dispatch);

    const value = {state, dispatch: enhancedDispatch};

    return <Store.Provider value={value}>{props.children}</Store.Provider>;
};

export {Store, StoreProvider};
